#include "cubacpp/cubacpp.hh"
#include <array>
#include <cmath>
#include <iostream>
#include <tuple>

// MyFunc is an example of a user-defined function to be integrated. It to be
// written as either a class or a struct. It must have const member function
// operator(), the function call operator, which takes one or more doubles (or
// types that can be converted to doubles).

class MyFunc {
public:
  explicit MyFunc(double mul) : multiplier(mul){};

  double
  operator()(double x, double y) const
  {
    return multiplier * x * y * (x + y);
  }

private:
  double multiplier;
};

double
freefun(double x, double y)
{
  return 3. * x * y * (x + y);
};

std::array<double, 3>
vfun(double x, double y)
{
  double f1 = x + y;
  double f2 = x * f1;
  double f3 = y * f1;
  return {{f1, f2, f3}};
}

// Here is an example main program that exercises our integration routines.
int
main()
{
  // We tell CUBA not to use mutiple processes. We intend our wrappers to be
  // used in MPI program, so parallelization through CUBA's internal forking
  // mechanism would cause conflicts with MPI.
  cubacores(0, 0);

  MyFunc ff{3.0};

  double constexpr epsrel = 1.0e-3;  // relative error goal
  double constexpr epsabs = 1.0e-12; // absolute error goal

  std::cout << std::scientific; // control output formatting of floating-point.

  // Our integration functions return a convergence status code, a value for the
  // integral, an uncertaintly, and a "chi-squared probability". We can unpack
  // them all on one line, using a "structured binding".
  auto [val, err, prob, neval, status] =
    cubacpp::VegasIntegrate(ff, epsrel, epsabs);
  std::cout << "Vegas -> "
            << "Value: " << val << " +/- " << err << " prob: " << prob
            << " neval: " << neval << " status: " << status << std::endl;

  // We can also just capture the integration_results struct as one object.
  auto res = cubacpp::CuhreIntegrate(ff, epsrel, epsabs);
  std::cout << "Cuhre has " << (res.converged() ? "" : "not ")
            << "converged.\n";
  std::cout << "Cuhre -> " << res << std::endl;

  // We can express the same integrand in a lambda expression, and pass it to
  // the integration routines.
  auto res2 = cubacpp::CuhreIntegrate(
    [](double x, double y) { return 3. * x * y * (x + y); }, epsrel, epsabs);
  std::cout << "Cuhre -> " << res2 << std::endl;
  auto res3 = cubacpp::CuhreIntegrate(&freefun, epsrel, epsabs);
  std::cout << "Cuhre -> " << res3 << std::endl;
  std::cout << '\n';
  auto hard = [](double x, double y, double z) {
    auto arg = x + y * y + z * z * z;
    return 1.0 / std::sqrt(arg) / 1.0882561118898;
  };
  double epsrel2 = 1.0e-3; // make the integration routines work harder...
  double epsabs2 = 1.0e-12;
  std::cout << "Vegas -> "
            << cubacpp::VegasIntegrate(
                 hard, epsrel2, epsabs, 0, 0, 2 * 1000 * 1000)
            << std::endl;
  std::cout << "Cuhre -> "
            << cubacpp::CuhreIntegrate(
                 hard, epsrel2, epsabs, 0, 0, 2 * 1000 * 1000)
            << std::endl;
  std::cout << "Suave -> "
            << cubacpp::SuaveIntegrate(
                 hard, epsrel2, epsabs, 0, 0, 2 * 1000 * 1000)
            << std::endl;
  std::cout << '\n';
  double constexpr pi = 0x1.921fb54442d18p+1;
  auto f29 = [](double x) {
    return 5. * pi * x * std::sin(3. * pi * x) * std::cos(2. * pi * x) / 3.;
  };
  std::cout << "Vegas -> " << cubacpp::VegasIntegrate(f29, epsrel2, epsabs2)
            << std::endl;
  std::cout << "Suave -> " << cubacpp::SuaveIntegrate(f29, epsrel2, epsabs2)
            << std::endl;

  // An example using a transformation of variables to integrate over
  // a range different from (0,1):
  constexpr double a = 1.0;      // lower bound
  constexpr double b = 10. * pi; // upper bound
  constexpr double scale = b - a;
  auto sinxx = [](double x) {
    auto const arg = scale * x + a;
    return scale * std::sin(arg) / arg;
  };
  std::cout << "For the following the correct value is " << 0.5929460092104
            << '\n';
  std::cout << cubacpp::VegasIntegrate(sinxx, 1.e-3, 1.e-12) << std::endl;
  std::cout << cubacpp::SuaveIntegrate(sinxx, 1.e-3, 1.e-12) << std::endl;

  cubacpp::Vegas v;
  v.maxeval = 100 * 1000 * 1000;
  std::cout << v.integrate(sinxx, 1.e-3, 1.e-12) << std::endl;
  cubacpp::Suave s;
  s.flags = 2;
  std::cout << s.integrate(sinxx, 1.e-3, 1.e-12) << std::endl;
  cubacpp::Cuhre c;
  c.key = 7;
  std::cout << c.integrate(hard, 1.e-3, 1.e-12) << std::endl;
  c.key = 9;
  std::cout << c.integrate(hard, 1.e-3, 1.e-12) << std::endl;
  std::cout << "Testing integration of vector-valued function\n";
  std::cout << c.integrate(vfun, 1.e-4, 1.e-12) << std::endl;
}
