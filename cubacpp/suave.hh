#ifndef CUBACPP_SUAVE_HH
#define CUBACPP_SUAVE_HH

#include "cuba.h"
#include "cubacpp/arity.hh"
#include "cubacpp/integrand.hh"
#include "cubacpp/integrand_traits.hh"
#include "cubacpp/integration_result.hh"

namespace cubacpp {

  template <class F>
  integration_result
  SuaveIntegrate(F const& f,
                 double epsrel,
                 double epsabs,
                 int flags = 0,
                 long long mineval = 0,
                 long long maxeval = 50000,
                 long long nnew = 1000,
                 long long nmin = 2,
                 double flatness = 25)
  {
    integrand_t igrand = detail::integrand<F>;
    constexpr int nvec = 1;
    int nregions = 0;
    long long neval = 0;
    int fail = 0;
    double val = 0.0;
    double err = 0.0;
    double prob = 0.0;
    llSuave(detail::integrand_traits<F>::ndim,
            1,
            igrand,
            (void*)&f,
            nvec,
            epsrel,
            epsabs,
            flags,
            0, // seed
            mineval,
            maxeval,
            nnew,
            nmin,
            flatness,
            nullptr,
            nullptr,
            &nregions,
            &neval,
            &fail,
            &val,
            &err,
            &prob);
    return {val, err, prob, neval, fail};
  }

  struct Suave {
    int flags = 0;
    long long int mineval = 0;
    long long int maxeval = 50000;
    long long int nnew = 1000;
    long long int nmin = 2;
    double flatness = 25.0;

    template <class F>
    integration_result
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return SuaveIntegrate(f,
                            epsrel,
                            epsabs,
                            flags,
                            mineval,
                            maxeval,
                            nnew,
                            nmin,
                            flatness);
    }
  };
}

#endif
